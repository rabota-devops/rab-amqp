const amqp = require('amqplib/callback_api');

const {RAB_MQ_USERNAME, RAB_MQ_PASSWORD, RAB_MQ_PORT_5672_TCP_ADDR, RAB_MQ_PORT_5672_TCP_PORT} = process.env;

/**
 * Connect to amqp channel and supply helper functions
 * @param address
 * @param exchange
 * @param appName
 * @param onConnect
 */
const connect = ({
    address,
    exchange,
    appName,
    onConnect
}) => {
    const ex = exchange || 'amq.topic';
    const app = appName || '';
    const host = address || RAB_MQ_PORT_5672_TCP_ADDR ? `amqp://${RAB_MQ_USERNAME}:${encodeURIComponent(RAB_MQ_PASSWORD)}@${RAB_MQ_PORT_5672_TCP_ADDR}:${RAB_MQ_PORT_5672_TCP_PORT}` : '';

    amqp.connect(host, (err, conn) => {
        conn.createChannel((err, ch) => {
            ch.assertExchange(ex, 'topic', {durable: true});

            /**
             * Create a queue
             * @param q
             * @param key
             * @param onConnect
             */
            const assertQueue = (q, key, onConnect) => {
                ch.assertQueue(q, {exclusive: false}, (err, q) => {
                    ch.bindQueue(q.queue, ex, key);

                    console.log(` [*] Waiting for messages on ${key}`);

                    ch.consume(q.queue, ({content, fields: { routingKey }}) => {

                        const parsedContent = JSON.parse(content);

                        consumeLogger(routingKey, content);

                        onConnect(parsedContent);

                    }, {noAck: true});
                })
            };

            /**
             * Send the intent to apps (ex. Moes or Calendar api)
             * @param message
             * @param key
             */
            const sendMessage = (message, key) => {
                ch.publish(ex, key, new Buffer(JSON.stringify({
                    ...message,
                    app
                })));

                console.log(" [x] => %s:'%s'", key, JSON.stringify(message));
            };

            /**
             * Log messages
             * @param routingKey
             * @param message
             */
            const consumeLogger = (routingKey, message) => console.log(" [x] <= %s:'%s'", routingKey, message);

            /**
             * Trigger onConnect
             */
            onConnect({assertQueue, sendMessage, err});
        });
    });
};

module.exports = connect;